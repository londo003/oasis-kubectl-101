Local Kubernetes Access in Oasis
---

This document demonstrates how to access Kubernetes clusters, including the current DHE Openshift, and future DKS Azure K8s, from your local machine.

### Kubectl Config

The first thing that needs to be done to access any kubernetes cluster is to [install kubectl](https://kubernetes.io/docs/tasks/tools/). Note, if you have installed [Rancher Desktop](https://gitlab.oit.duke.edu/londo003/spike-rancher-desktop) you may already have kubectl installed.

Kubectl manages a configuration on your local machine, stored in $HOME/.kube/config, by
default. This can be [overridden](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/),
using arguments to kubectl, or a KUBECONFIG environment variable. This file can be modified 
using kubectl commands to add and remove contexts, so keep regular backups to ensure 
continued access.

#### Setting up a kubectl context

Contexts allow you to associate a single named entity with a specific kubernetes cluster,
access credential, and namespace. You can have multiple contexts to different namespaces on
the same cluster, or in different clusters. Once you have set the kubectl context, all
kubectl commands will default to using the configured cluster, access credentials, and
namespace, unless overridden with explicit kubectl arguments.

Steps:
1. define a new cluster (this only has to be done once per cluster)
```
kubectl config set-cluster dhe_openshift --server= ${CLUSTER_SERVER}
```
Where CLUSTER_SERVER is the address to the k8s cluster, which will have been sent to you
with your HELM_TOKEN when your namespace was created.

2. define a new set of access credentials. We will typically use the HELM_USER and 
HELM_TOKEN sent to us in the email we received when our namespace was created to
access the DHE Openshift cluster. The HELM_TOKEN is long-lived, and can be used to
access the cluster at any time without renewal. You can also get a more temporary token
for your netid user using the Openshift GUI, or the [azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli). This will expire relatively quickly.

```
kubectl config set-credentials "${HELM_USER}" --token="${HELM_TOKEN}"
```

3. Create a new context to associate a cluster, namespace, and user
```
kubectl config set-context ${PROJECT_NAMESPACE} --cluster=dhe_openshift --namespace=${PROJECT_NAMESPACE} --user=${HELM_USER}
```

You can use any string for the first argument after `set-context`, it does not have to be
a namespace name.

4. List your contexts, or show your current context
```
kubectl config get-contexts
kubectl config current-context
```

5. Set the context used by all kubectl commands (across all terminals and sessions)
```
kubectl config use-context ${PROJECT_NAMESPACE}
```

You can do all of the above using scripts in the [Oasis Deployment System](https://gitlab.dhe.duke.edu/ori-rad/ci-pipeline-utilities/deployment/).

1. clone the deployment repo
```
cd $HOME
mkdir lib
cd lib
git clone git@gitlab.dhe.duke.edu:ori-rad/ci-pipeline-utilities/deployment.git
cd $HOME
```

2. create a file that sets the CLUSTER_SERVER, PROJECT_NAMESPACE, HELM_USER, and HELM_TOKEN
environment variables. This file can explicitly export these:
```
export CLUSTER_SERVER=...
export PROJECT_NAMESPACE=...
export HELM_USER=...
export HELM_TOKEN=...
```

These can be useful in other contexts, such as feeding as --env-file to docker or
kubectl commands, without explicitly exporting the variables:
```
CLUSTER_SERVER=...
PROJECT_NAMESPACE=...
HELM_USER=...
HELM_TOKEN=...
```

3. source the above file
If exported
```
source $file
```

If not exported
```
set -a
source $file
set +a
```

4. Source the local helper library in the deployment repo
```
source $HOME/lib/deployment/lib/helpers/local
```

5. login to the cluster, namespace, user, and token
```
cluster_login
```

#### Useful Kubectl commands

[Here](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) is a kubectl cheatsheet.

Kubectl has extensive help
```
kubectl --help
kubectl run --help
kubectl config --help
```

Run an interactive bash session in a pod with a TTY in the current context:
```
kubectl run bash-test \
-t -i --leave-stdin-open=true \
--image=gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:1.7.6 \
--env="CLUSTER_SERVER=${CLUSTER_SERVER}" \
--env="HELM_USER=${HELM_USER}" \
--env="HELM_TOKEN=${HELM_TOKEN}" \
--env="PROJECT_NAMESPACE=${PROJECT_NAMESPACE}" \
 --command -- /bin/bash
If you don't see a command prompt, try pressing enter
bash-5.1$ source /usr/local/lib/helpers/local
bash-5.1$ cluster_login 
Cluster "ci_kube" set.
User "helm-deployer" set.
Context "londo003-deploy" created.
Switched to context "londo003-deploy".
bash-5.1$ kubectl get pods
NAME                                                              READY   STATUS    RESTARTS   AGE
bash-test
...
bash-5.1$ exit
Session ended, resume using 'kubectl attach bash-test -c bash-test -i -t' command when the pod is running
kubectl attach bash-test -c bash-test -i -t
If you don't see a command prompt, try pressing enter.
bash-5.1$ 
```

delete bash-test
```
kubectl delete pod/bash-test
```

list pods (with/without labels)
```
kubectl get pods
kubectl get pods -lapp=departmental-routing
kubectl get pods -lapp=departmental-routing,environment=review-ir-733-nav-jl8com
```
(same is used for deployments, statefulsets, secrets, etc)

show pod configuration in json format (with/without jq)
```
kubectl get pod/review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v -o json
kubectl get pod/review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v -o json | jq
kubectl get pod/review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v -o json | jq -r '.metadata.labels'
```

exec into an existing pod (this creates a completely new shell session inside the container). This can be done multiple times
for multiple separate shell sessions
```
kubectl exec -t -i  pod/review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v -- /bin/bash
Defaulted container "departmental-routing" out of: departmental-routing, departmental-routing-db-migrate (init), departmental-routing-db-seed (I have no name!@review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v:/opt/app-root/src$ rails c
irb(main):002:0> Period.count
=> 26
irb(main):003:0> exit
I have no name!@review-ir-733-nav-menu-ch-gu3tio-departmental-routing-5b7bmlx2v:/opt/app-root/src$ exit
```

Note, if your pod has multiple containers, you will need to specify an exact container name with -c.

### Octant

[Octant](https://octant.dev/) is an Open Source Browser Application that allows visual interaction with
kubernetes cluster contexts defined in your $HOME/.kube/config (or $KUBECONFIG). It has the same features
as the Openshift browser UI, such as:
- listing, and editing deployments, secrets, configmaps, etc.
- printing out the logs of a pod
- attaching to a terminal session in a pod
- seeing events

[getting started](https://reference.octant.dev/?path=/story/docs-intro--page)

### Publishing an Image to a gitlab container image registry for use with kubectl run

You may want to create an image that you can use in a kubectl run session. Ideally, you should tag and publish the image
to a public gitlab repo, so that you do not have to create a image pull secret.

1. create the image locally, with a tag to the gitlab registry url you want to pull (e.g. one of $CI_REGISTRY_IMAGE:$TAG, $CI_REGISTRY_IMAGE/$IMAGE_NAME:$TAG, or $CI_REGISTRY_IMAGE/$APP/$IMAGE:$TAG, see [gitlab docs](https://gitlab.dhe.duke.edu/help/user/packages/container_registry/index) for details).
```
docker build -t gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:test .
```
alternatively, you can tag an existing image
```
docker tag $existing gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:test
```

2. login to gitlab.dhe.duke.edu:4567 with a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with write_registry scope.
```
cat $file_with_my_access_token | docker login -u $netid --password-stdin gitlab.dhe.duke.edu:4567
```

3. push the tagged image
```
docker push gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:test
```
